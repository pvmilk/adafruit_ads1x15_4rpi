
#include <iostream>
#include <unistd.h>

#include "Adafruit_ADS1015.h"

using namespace std;

/**
  */
int main(){
    Adafruit_ADS1015 ads1015;

    uint16_t adc;

    while(true){
        adc = ads1015.readADC_SingleEnded(0);  

        cout << static_cast<unsigned int>(adc)*0.003 << endl;
        usleep(100000);
    }
    return 0;
}

