TARGET           = main
TEMPLATE         = app
CONFIG          -= qt   #exclude the linkage to -lQtGUI and -lQtCore

INCLUDEPATH     += ./

OBJECTS_DIR     += .obj

#LIBS           += -lXXX

QMAKE_CXXFLAGS  +=-g -std=c++0x

HEADERS         += Adafruit_ADS1015.h

SOURCES         += main.cpp \
                   Adafruit_ADS1015.cpp
                   


