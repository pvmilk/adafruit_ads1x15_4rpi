Modify the original code to use with Raspberry Pi.
    - original git repository (https://github.com/adafruit/Adafruit_ADS1X15)

Milk Phongtharin

Adafruit_ADS1015
================

Driver for TI's ADS1015: 12-bit Differential or Single-Ended ADC with PGA and Comparator
